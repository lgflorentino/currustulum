FROM rust:1.72-bookworm

RUN apt-get -y update
RUN apt-get -y upgrade
RUN apt-get install sudo
RUN apt-get install zlib1g-dev zlib1g
RUN apt-get install libncurses-dev
RUN apt-get install -y llvm-dev clang libclang-dev
RUN apt-get install -y bash zsh

ARG WORK_D=/currustulum/
ARG USER_ID=1000
ARG GROUP_ID=1000

RUN groupadd -g $GROUP_ID rusty
RUN useradd -m -s /bin/zsh -g $GROUP_ID -G sudo,dialout,staff,adm -u $USER_ID rusty

RUN mkdir -p $WORK_D

COPY . $WORK_D

WORKDIR $WORK_D

USER rusty

#RUN cargo install --path ./

RUN cargo install trunk

RUN rustup target add wasm32-unknown-unknown

