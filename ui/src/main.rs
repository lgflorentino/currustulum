
use yew::prelude::*;
use gloo_net::http::{Request, Response};
use url::{Url, ParseError};

#[function_component(App)]
fn app() -> Html {
    let ac_base_url = Url::parse("https://rdf.australiancurriculum.edu.au/api/sparql");
    let ac_json_url = Url::parse("https://australiancurriculum.edu.au/static/elements/2018/05/7f6bd186-fcdf-4e46-a727-9e4600a2a39b-manifest.json").unwrap();

    let query_1 = Request::get(ac_json_url.as_str()).send();
    
/*    use_effect_with((), move |_| {
        wasm_bindgen_futures::spawn_local(async move {    
            let query: String = Request::get(ac_json_url.as_str())
            .send()
            .await
            .unwrap()
            .json()
            .await
            .unwrap();
            println!("{:?}", query);
        })
    });
    */
    html! {
        <h1>{ "Hello World!" }</h1>
    }
}

fn main() {
    yew::Renderer::<App>::new().render();
}
