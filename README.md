currustulum
---

This is the source code for a webapp to visualize the Australian Curriculum linearly.
The webapp can be accessed at this URL: [Currustulum Prod](https://)

This source code of the Currustulum webapp is licensed under the MIT License, a copy of which can be found in the LICENSE file in this repository.

The webapp and the webapp author are in no way associated with Australian Curriculum, Assessment and Reporting Authority (ACARA). 

There is absolutely no guarantee that any of the information provided is correct. 
There is absolutely no guarantee that any of the information provided is relevant to you or your circumstances. 

The information displayed is acquired via the Machine Readable Australian Curriculum from the following URL: (RDF Australian Curriculum)[https://rdf.australiancurriculum.edu.au/]. 
The copyright of the RDF resource belongs to © Australian Curriculum, Assessment and Reporting Authority (ACARA) and the terms of use of the RDF resource can be found at this URL: (Terms of Use)[https://www.australiancurriculum.edu.au/copyright-and-terms-of-use/] 

